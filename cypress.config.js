const { defineConfig } = require('cypress')

const fs = require('fs-extra')
const path = require('path')

function getConfigurationByFile(file) {
  const pathToConfigFile = path.resolve('./', 'cypress/config', `${file}.config.json`)
  return fs.readJson(pathToConfigFile)
}

module.exports = defineConfig({
  chromeWebSecurity: false,
  defaultCommandTimeout: 30000,
  viewportWidth: 1920,
  viewportHeight: 1080,
  reporter: 'cypress-mochawesome-reporter',
  video: false,
  screenshotOnRunFailute: true,
  reporterOptions: {
    reportDir: 'cypress/reports',
    charts: false,
    reportPageTitle: 'QAA-Dispatch',
    embeddedScreenshots: true,
    jsonReport: true,
    htmlReport: false,
  },
  experimentalStudio: true,
  env: {
    dispatchUser: 'DISPATCHER@QA.COM',
    dispatchUserPassword: 'Test1234',
    passengerFirstName: 'testQA',
    passengerLastName: 'testQA',
    passengerEmail: 'qatest@qatest.com',
    pickupLoc: 'Las Vegas, NV, USA',
    pickupLocOptn: 'Las Vegas',
    dropLoc: 'Las Vegas Strip, NV, USA',
    dropLocOptn: 'Las Vegas Strip',
    phoneNumber: '+50625367584',
    dispatcherReservationNo: 'RES-12Z45P',
    accountName: 'Circa - Testing',
    accountID2: 'Circa - Testing  ',
    couponCode: 'ERASE10D',
    searchEmail: 'dispatcher@qa.com',
    dispatchPhoneNumber: '+12015567203',
    lastName: 'Dispatcher',
    lastName1: 'Dispatcher1',
    license: 'HARBINGER1',
    make: 'Tesla',
    make1: 'Tesla1',
    dispatcheremailstart: 'admin',
    dispatcheremailend: '@QA.com',
    dispatchLastname: 'QA',
    dispatchphonedialcode: '+1 516-787-',
    dispatchPassword: 'Senthil123',
    dispatchConfirmPassword: 'Senthil123',
    popupMessage: ' Thank you for signing up! ',
    Homepage: ' Kaptyn Las Vegas ',
    companydetailsreferralcode: '105F6J',
    adminaccount: 'new@admin.com',
    dispatchersaccount: 'test1@qa.com',
    sizes: [[1920, 1080]]
  },
  e2e: {
    // We've imported your old cypress plugins here.
    // You may want to clean this up later by importing these.
    setupNodeEvents(on, config) {
      const file = config.env.configFile || 'staging'
      return getConfigurationByFile(file)
    },
  },
})
