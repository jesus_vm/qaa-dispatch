/// <reference types="cypress"/>

describe('Driver List and Edit', () => {
  Cypress.env('sizes').forEach((size) => {
    context('Search for a driver and update lastname', () => {

      it(`Validate Driver list and update driver lastname ${size}`, () => {

        const dispatcherLastname = 'Dispatcher'
        const dispatcherLastnameUpdate = 'Dispatcher updated'

        cy.visit(Cypress.env('KaptynSiteUrl'));
        cy.loging({
          dispatcherUser: Cypress.env('dispatchUser'),
          disptacherUserPassword: Cypress.env('dispatchUserPassword'),
        });
        cy.get('#navSidebar').find('img').should('be.visible');

        //Code for searching driver by Email
        cy.driverList({
          searchByEmail: Cypress.env('searchEmail'),
        });

        //Code for validating driver's phone number
        cy.validatDriverPhoneNumber(Cypress.env('dispatchPhoneNumber'));
          //Code for updating last name
        cy.get('#lastName').clear();  
        cy.get('#lastName').type(dispatcherLastnameUpdate);
        cy.get('.action-buttons > .btn-success').click();
        cy.wait(3000);

        //Code for validating last name changed
        cy.validateDriverLastName(dispatcherLastnameUpdate);
        cy.get('.close').click();
        cy.wait(3000)
        cy.reload();

        //Code for searching same driver by Email again
        cy.driverList({
          searchByEmail: Cypress.env('searchEmail'),
        });

        //Code for updating last name to original value
        cy.get('#lastName').clear();
        cy.get('#lastName').type(dispatcherLastname);
        cy.get('.action-buttons > .btn-success').click();
        cy.wait(3000)

        //Code for validating last name changed to original value
        cy.validateDriverLastName(dispatcherLastname);
        cy.get('.close').click();
        
      });
    });
  });
});
