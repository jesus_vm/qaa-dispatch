/// <reference types="cypress"/>

import moment from "moment";
describe('Ride List Filter', () => {

  Cypress.env('sizes').forEach((size) => {
    it(`Ride List Filter for viewport ${size}`, () => {

      context("Create New Coupon", () => {
        cy.visit(Cypress.env('KaptynSiteUrl'));

        cy.loging({
          dispatcherUser: Cypress.env('dispatchUser'),
          disptacherUserPassword: Cypress.env('dispatchUserPassword')
        });
        cy.wait(3000)
        cy.get('#navSidebar').find('img').should('be.visible');
        cy.wait(3000)
        cy.get('.jump-to-date > .form-control').click({ force: true });
        //Selecting Today filter for the rides
        cy.get('.start0.end0').contains('Today').click({ force: true });
        cy.wait(2000)
        // Itirate through the grid list and validate all the row have today's date
        let countOfEle = 0;
        cy.get('[class="ui-grid-row ng-scope"]').then($elements => {
          countOfEle = $elements.length;
          cy.log(countOfEle);
          for (let i = 1; i <= countOfEle; i++) {
            cy.log(i);
            cy.get(':nth-child(' + i + ') > .ng-isolate-scope > .ride-row > .ui-grid-coluiGrid-0006 > .ui-grid-cell-contents').invoke('text')
              .then(($currentvalue) => {
                //cy.log($currentvalue);
                const date = moment();
                expect($currentvalue).contain(date.format("ddd, MMMM DD"));
              })
          }
        })

        // Again setting the filter back to default state
        cy.get('.jump-to-date > .form-control').click({ force: true });
        cy.get('.clear').click({ force: true });

      });
    });
  });
});