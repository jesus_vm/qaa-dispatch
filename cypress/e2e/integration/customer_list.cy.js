/// <reference types="cypress"/>

describe('Customer List and Edit', () => {
  Cypress.env('sizes').forEach((size) => {
    context('Search for a customer and update lastname', () => {

      it(`Validate Customer list and update customer lastname ${size}`, () => {
         
        const customerLastname = 'Dispatcher';
        const customerLastnameUpdated = 'Dispatcher updated';

        cy.visit(Cypress.env('KaptynSiteUrl'));
        cy.loging({
          dispatcherUser: Cypress.env('dispatchUser'),
          disptacherUserPassword: Cypress.env('dispatchUserPassword'),
        });
        cy.get('#navSidebar').find('img').should('be.visible');
        //Code for searching customer by Email
        cy.customerList({
          searchEmail: Cypress.env('searchEmail'),
        });

        //Code for validating customers's phone number
        cy.get('.customer-info > .btn').click();
        cy.validatCustomerPhoneNumber(Cypress.env('dispatchPhoneNumber'));

        //Code for updating last name
        cy.get('.no-padding > :nth-child(3) > .form-control').clear();
        cy.get('.no-padding > :nth-child(3) > .form-control').type(customerLastnameUpdated);
        cy.get('.customer-info-container > .form > .form-inline > .col-xs-12 > .btn-success').click({ force: true });

        cy.wait(3000);

        cy.get('.customer-info > .btn').click()
        //Code for Assertion last name changed to dispatchcustomerlastname+1
        cy.validateCustomerLastName(customerLastnameUpdated);


        //Code for updating last name to original value

        cy.get('.no-padding > :nth-child(3) > .form-control').clear();
        cy.get('.no-padding > :nth-child(3) > .form-control').type(customerLastname);
        cy.get('.customer-info-container > .form > .form-inline > .col-xs-12 > .btn-success').click({ force: true });

        cy.wait(3000)

        //Code for validating last name changed to original value
        cy.validateCustomerLastName(customerLastname);
        cy.get('button.close').click();


      });
    });
  });
});