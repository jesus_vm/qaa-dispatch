describe('Verify Affiliates list', () => {
     Cypress.env('sizes').forEach((size) => {
        it('Verify filter and Affiliate list are working', () => {
            const phone_number=' +1 902-440-4418  ';
            cy.visit(Cypress.env('KaptynSiteUrl'));
            cy.loging({
              dispatcherUser: Cypress.env('dispatchUser'),
              disptacherUserPassword: Cypress.env('dispatchUserPassword'),
            });
            cy.get('#navSidebar').find('img').should('be.visible');
            cy.get('[id="affiliateBtn"]').click();
            cy.get('#affiliate_search_bar').type('CIRCA - Testing');
            cy.get('#affiliate_list_search_button > .glyphicon').click();
            cy.wait(2000);
            cy.get('[class="ui-grid-canvas"]').click();
            cy.get('[ng-if="$ctrl.affiliate.affiliate.group.settings.affiliate.phoneInternationalFormat"]').should('have.text',phone_number);
        });
    });
 });
