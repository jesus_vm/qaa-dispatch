/// <reference types="cypress"/>

describe('Verify Receipt Information', () => {

    it('Create Ride - Change Status - Charge and Validate Receipt', () => {
        
        cy.visit(Cypress.env('KaptynSiteUrl'));
        cy.wait(3000);
        cy.loging({
            dispatcherUser: Cypress.env('dispatchUser'),
            disptacherUserPassword: Cypress.env('dispatchUserPassword')
        });
        
        cy.newBooking({ cardNumber: false, serviceLevel: 'Premium Sedan Production', pickupLoc: 'Luxor Hotel & Casino'});
        cy.changeRideStatus();

        cy.get('.ng-scope.ng-isolate-scope > .btn-group > .btn').click();
        cy.wait(5000)
        cy.get('.quote-item').click();
        cy.get('.estimated-total > .ng-binding').invoke('text').then((text) => {
            const quote = text;
            cy.get('.modal-header > .close').click();

            //charge 
            cy.get('.action-buttons > [dr-unless="concierge"]').click();
            //view receipt 
            cy.get('.btn-default.ng-scope').click();

            cy.get('#paymentModal > .scroll-less-container > .header-banner > .title > .ng-binding').invoke('text').then((text) => {
                const res = text.split(' ')[1];
                cy.get('.modal-body > iframe').invoke('attr', 'src').then((href) => {
                    //Go To the iframe 
                    cy.visit(href);
                    cy.wait(3000);
                    cy.get(':nth-child(2) > td > b').invoke('text').then((text) => {
                        //validate ReservationNumber 
                        expect(text).to.eq(res);
                    })
                    
                    cy.get('[style="padding: 2px 12px 14px 9px;"] > [width="100%"] > :nth-child(1) > :nth-child(1) > .row-money').invoke('text').then((text) => {
                        //validate Total 
                        const totalExpected = parseFloat(quote.replace(/[`~!@#$%^&*()_|+\-=?;:'",<>\{\}\[\]\\\/]/gi, ''))
                        const totalGeted = parseFloat(text.replace(/[`~!@#$%^&*()_|+\-=?;:'",<>\{\}\[\]\\\/]/gi, '').replace(' ','').replace('\n', ""))
                        
                        expect(totalGeted).to.eq(totalExpected);
                    })

                })

            })

        })
    })


});
