/// <reference types="cypress"/>

describe('Settings', () => {
    Cypress.env('sizes').forEach((size) => {


        it(`Settings for viewport ${size}`, () => {

            cy.visit(Cypress.env('KaptynSiteUrl'));
            cy.loging({
                dispatcherUser: Cypress.env('dispatchUser'),
                disptacherUserPassword: Cypress.env('dispatchUserPassword'),
            });
            cy.get('#navSidebar').find('img').should('be.visible');

            // validation for Referral code in Company details
            cy.validatereferralcode(Cypress.env('companydetailsreferralcode'));


            // validation for email and SMS checkbox in Notifications
            cy.get('#notificationsSettingsBtn').click();
            cy.get('[ng-model="$ctrl.group.notifications.customer_ride_confirmation_on_create.email"]').check().should('be.checked')
            cy.get('[ng-model="$ctrl.group.notifications.customer_ride_confirmation_on_create.mobile"]').check().should('be.checked')

            //Validation for Administrators Account
            cy.validateadminaccount(Cypress.env('adminaccount'));

            //Validation for Dispatchers Account
            cy.validateadmindispatcheraccount(Cypress.env('dispatchersaccount'));
        });

    });
});