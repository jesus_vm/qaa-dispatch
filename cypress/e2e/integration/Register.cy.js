/// <reference types="cypress"/>

var emailid

describe('Register', () => {
 Cypress.env('sizes').forEach((size) => {
    it(`Register for Kops in different viewport `, () => {
      cy.visit(Cypress.env('KaptynSiteUrl'));

      cy.get('.loginForm').contains('Log In').should('be.visible');
      cy.get('.loginIcon').find('img').should('be.visible');

      cy.loging({
        dispatcherUser: Cypress.env('dispatchUser'),
        disptacherUserPassword: Cypress.env('dispatchUserPassword')
      });
        //Code for Register
        cy.get('#navSidebar').find('img').should('be.visible');
        cy.get('li#li-module.ng-scope').contains(' Settings ').click();
        cy.get('#adminsBtn').contains(' Administrators ').click();
        //New Admin mail creation with that day's date
        cy.admin({
          dispatcheremailstart: Cypress.env('dispatcheremailstart'),
          dispatcheremailend: Cypress.env('dispatcheremailend')
        });


        //validation for new admin
        cy.wait(5000);
        let countOfElements =  0;
        cy.get('div[class="input-group ng-scope"] [ng-click="whitelists.removeAdminEmail(email)"] [class="glyphicon glyphicon-remove"]').then($elements => {
        countOfElements = $elements.length;
        cy.get(':nth-child(3) > .panel > .panel-body > :nth-child('+countOfElements+') > .form-control')
        //Logout
        cy.get('.glyphicon.glyphicon-chevron-down').click();
        cy.get('a.ng-binding.ng-scope').contains('Logout ').click();
        //click on Create an account
        cy.get(':nth-child(4) > a').click();


        //create an account page data
        cy.addadminDetails({
        dispatcheremailstart: Cypress.env('dispatcheremailstart'),
        dispatchLastname: Cypress.env('dispatchLastname'),
        dispatchphonedialcode: Cypress.env('dispatchphonedialcode'),        
        dispatchPassword: Cypress.env('dispatchPassword'),
        dispatchConfirmPassword: Cypress.env('dispatchConfirmPassword')
        });
        
        //validation for Thank You signung in Popup
        cy.validatePopUpMessage(Cypress.env('popupMessage'))
        //click back to login
        cy.get('p > a').contains('Back to Login').click();
        
        
        //Login with new data
        cy.logingnew({  
          dispatchPassword: Cypress.env('dispatchPassword')
        });
        //validation for Login successfully
        cy.validateloginsuccessfully(Cypress.env('Homepage'))
        //Click settings & Administrator
        // cy.get('li#li-module.ng-scope').contains(' Settings ').click();
        // cy.get('#adminsBtn').contains(' Administrators ').click();

        //Remove created new Administrator
        //cy.get(':nth-child(38) > .input-group-btn > .btn > .glyphicon')
        // cy.get(':nth-child(38) > .form-control').click();
       
        });
        cy.wait(4000);
        
        cy.loging({
          dispatcherUser: Cypress.env('dispatchUser'),
          disptacherUserPassword: Cypress.env('dispatchUserPassword')
        });
        cy.get('li#li-module.ng-scope').contains(' Settings ').click();
        cy.get('#adminsBtn').contains(' Administrators ').click();
        cy.Verifyemailpresentassertion({});
    });
   
  });
});
