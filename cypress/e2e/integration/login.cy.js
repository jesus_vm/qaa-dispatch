/// <reference types="cypress"/>

describe('Disptacher Login', () => {
  Cypress.env('sizes').forEach((size) => {
    it(`Loging for viewport ${size}`, () => {
      cy.visit(Cypress.env('KaptynSiteUrl'));

      cy.get('.loginForm').contains('Log In').should('be.visible');
      cy.get('.loginIcon').find('img').should('be.visible');

      cy.loging({
        dispatcherUser: Cypress.env('dispatchUser'),
        disptacherUserPassword: Cypress.env('dispatchUserPassword')
      });

      cy.get('#navSidebar').find('img').should('be.visible');
    });
  });
});
