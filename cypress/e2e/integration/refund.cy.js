/// <reference types="cypress"/>

describe('Refund ', () => {

    it('Create Ride - Change Status - Charge and Refund', () => {

        cy.visit(Cypress.env('KaptynSiteUrl'));
        cy.wait(3000);
        cy.loging({
            dispatcherUser: Cypress.env('dispatchUser'),
            disptacherUserPassword: Cypress.env('dispatchUserPassword')
        });
        
        cy.newBooking({ cardNumber: false, serviceLevel: 'Premium Sedan Production', pickupLoc: 'Luxor Hotel & Casino'});
        cy.changeRideStatus();

        cy.get('.ng-scope.ng-isolate-scope > .btn-group > .btn').click();
        cy.wait(5000)
        cy.get('.quote-item').click();
        cy.get('.estimated-total > .ng-binding').invoke('text').then((text) => {
            let quote = text;
            cy.get('.modal-header > .close').click();

            //charge 
            cy.get('.action-buttons > [dr-unless="concierge"]').click()
            cy.contains('Charge').click().then(() => {
                //get current value
                cy.get('.paid-amount').invoke('text').then((text) => {
                    //get current value from card and convert to number
                    const totalExpected = quote.replace(/[`~!@#$%^&*()_|+\-=?;:'" ,<>\{\}\[\]\\\/]/gi, '')
                    const totalText = text.replace(/[`~!@#$%^&*()_|+\-=?;:'" ,<>\{\}\[\]\\\/]/gi, '').replace(' ','');
                    expect(totalText.replace( /^\D+/g, '')).to.eq(totalExpected);
                    //refund 
                    cy.get('[ng-click="$ctrl.openInitiateRefundModal()"]').click();
                    cy.wait(2000);
                    //Convert to number 
                    quote = parseFloat(quote.replace(/[`~!@#$%^&*()_|+\-=?;:'" ,<>\{\}\[\]\\\/]/gi, ''));
                    //Put refund value Current Quote - 10$
                    cy.get('input[ng-model="transaction.refundAmount"]').type(`${quote - 10}`);
                    cy.get(`button[ng-click="$ctrl.refundTransaction(transaction, transaction.captured ? $ctrl.credits[transaction._id + 'refundAmount'] : transaction.amounts.net)"]`).click();
                    cy.wait(3000);
                    cy.get('.modal-header > .close').click();
                    cy.wait(5000);
                    //Validate that refund was success
                    cy.get('.paid-amount').invoke('text').then((updatedText) => {
                        const updatedTotal = updatedText.replace(/[`~!@#$%^&*()_|+\-=?;:'" ,<>\{\}\[\]\\\/]/gi, '').replace(' ','');
                        expect(parseFloat(totalExpected)).to.above(parseFloat(updatedTotal.replace( /^\D+/g, '')))
                    });
                });
            });
        });
    });
});
