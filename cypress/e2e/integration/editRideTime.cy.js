/// <reference types="cypress"/>

describe('Update estimated ride duration', () => {
  Cypress.env('sizes').forEach((size) => {
    it(`Edit a ride and update the estimated ride duration ${size}`, () => {
      const rideUpdatedHour = '10';
      const rideNormalHour = '9'
      const reservationNumber = 'RES-12Z45P';

      cy.visit(Cypress.env('KaptynSiteUrl'));

      cy.get('.loginForm').contains('Log In').should('be.visible');
      cy.get('.loginIcon').find('img').should('be.visible');

      cy.loging({
        dispatcherUser: Cypress.env('dispatchUser'),
        disptacherUserPassword: Cypress.env('dispatchUserPassword'),
      });

      cy.filterRides({
        dispatcheReservationNo: reservationNumber,
      });

      cy.changeRidePickupTime({
        updatedhour: rideUpdatedHour,
      });

      //update and close
      cy.get('.fa.fa-check').click();
      cy.wait(5000);
      cy.get('.header-close > .close').click();

      //click same ride
      cy.get('.ui-grid-canvas > .ng-scope').click({ multiple: true });
      cy.wait(5000);

      // validate updated time
      cy.validateUpdatedTime(rideUpdatedHour);

      //go for previous time and update
      cy.changeRidePickupTime({
        updatedhour: rideNormalHour,
      });

      cy.get('.fa.fa-check').click();
    });
  });
});
