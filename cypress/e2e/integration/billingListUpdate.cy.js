/// <reference types="cypress"/>

describe('Verify Billing List and updates', () => {
  Cypress.env('sizes').forEach((size) => {
    it(`Verifying unpaid reservation from Billing for viewport ${size}`, () => {
      cy.visit(Cypress.env('KaptynSiteUrl'));

      cy.get('.loginForm').contains('Log In').should('be.visible');
      cy.get('.loginIcon').find('img').should('be.visible');

      cy.loging({
        dispatcherUser: Cypress.env('dispatchUser'),
        disptacherUserPassword: Cypress.env('dispatchUserPassword')
      });

      cy.get('#navSidebar').find('img').should('be.visible');
      cy.get('#billingBtn').click();

      //Code for validating reservation number for unpaid ride
      cy.filterBilling({
        ReservationNo: Cypress.env('dispatcherReservationNo'),
      });

    });
  });
});
