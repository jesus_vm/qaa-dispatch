/// <reference types="cypress"/>

import moment from "moment";
describe('Add/Edit/Delete Coupon', () => {
    
    Cypress.env('sizes').forEach((size) => {
      it(`Add/Edit/Delete Coupon for viewport ${size}`, () => {

        context("Create New Coupon", () => {
        cy.visit(Cypress.env('KaptynSiteUrl'));
  
        cy.loging({
          dispatcherUser: Cypress.env('dispatchUser'),
          disptacherUserPassword: Cypress.env('dispatchUserPassword')
        });
  
        cy.get('#navSidebar').find('img').should('be.visible');
        cy.wait(3000)
         // click on pricing
        cy.get('#pricingBtn').contains(' Pricing ').click({ force: true });
         // click on coupon codes
        cy.get('#couponsBtn').contains(' Coupon Codes ').click({ force: true });
         // click on New Coupon button
        cy.get('.sidelist-header > .btn').contains('New Coupon ').click({ force: true });
         // Validating the New Coupon code header
         cy.get(':nth-child(1) > .col-md-12 > .ng-scope').contains('New Coupon Code').should('have.text', "New Coupon Code"); 
         // Entering code as “ERASEME10D<MMDD>”
         const date = moment();
         const randomNumber = Math.floor((Math.random() * 10) + 1);
         var currentDate = date.format("MMDD");
         var res = Cypress.env('couponCode').concat(currentDate + randomNumber);
         cy.get(':nth-child(1) > :nth-child(1) > .ng-invalid-validate-not-empty').type(res);
         //Select $ and enter the same
         cy.get(':nth-child(2) > div.discount-type').click({ force: true });
         cy.get('.discount-value').type("10");
         //click on save button
         cy.get('.editor-footer > .btn-success').click({ force: true });
         cy.wait(2000);
         cy.reload();
         cy.wait(2000);
         cy.get(':nth-child(1) > .coupon-list-item > .coupon-info-container > .coupon-info').contains(res).should('have.text', res);


      });


      context("Delete the added Coupon", () => {
         //click on the added coupon and delete the same
         cy.get(':nth-child(1) > .coupon-list-item > .coupon-info-container > .coupon-info').click({ force: true });
         cy.get('.col-md-12 > .ng-binding').invoke('text').then((text) => {
          // validating the edit header
          expect(text).to.have.string('Editing')
        })
         cy.get('.btn-danger').click({ force: true });
         cy.get('.modal-footer > .btn-primary').contains('Yes').click({ force: true });
         cy.wait(2000);
         cy.reload();
         cy.wait(2000);
         cy.get(':nth-child(1) > .coupon-list-item > .coupon-info-container > .coupon-info').should('not.have.text', "ERASE10D");
      });
        
    });
  });
});