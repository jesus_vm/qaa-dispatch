/// <reference types="cypress"/>

describe('Verify Vehicle List and updates', () => {
    Cypress.env('sizes').forEach((size) => {
    it(`Verifying Vehicle details and License plate from vehicle screen for viewport ${size}`, () => {
        cy.visit(Cypress.env('KaptynSiteUrl'));

        const carName = 'Tesla'
        const carNameUpdated = 'Tesla Updated'

        cy.get('.loginForm').contains('Log In').should('be.visible');
        cy.get('.loginIcon').find('img').should('be.visible');

        cy.loging({
            dispatcherUser: Cypress.env('dispatchUser'),
            disptacherUserPassword: Cypress.env('dispatchUserPassword')
        });

        cy.get('#navSidebar').find('img').should('be.visible');

        //Validate license plate
        cy.vehicleList({
            licensePlate: Cypress.env('license'),
        });

        //Code for updating make name to make+1
        cy.get('[name="make"]').clear();
        cy.get('[name="make"]').type(carNameUpdated, {force: true});
        cy.get('.modal-footer > .action-buttons > .btn-success').click()
        cy.wait(3000);

        //Validate make changed
        cy.validateMake(carNameUpdated);
        cy.get('.close').click();
        cy.wait(3000)
        cy.reload();

        //Validate license plate
        cy.vehicleList({
            licensePlate: Cypress.env('license'),
        });

        //Code for updating make name to original value
        cy.get('[name="make"]').clear();
        cy.get('[name="make"]').type(carName, {force: true});
        cy.get('.modal-footer > .action-buttons > .btn-success').click()
        cy.wait(3000);

        //Validate make changed to original
        cy.validateMake(carName);
        cy.get('.close').click();
    });
    });
});


