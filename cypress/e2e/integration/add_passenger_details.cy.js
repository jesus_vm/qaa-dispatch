describe('Booking Confirmation Test', () => {
  Cypress.env('sizes').forEach((size) => {
    context('Test passenger fields modal', () => {
      
      it('Write passenger information', () => {
        cy.visit(Cypress.env('KaptynSiteUrl'));

        cy.loging({
          dispatcherUser: Cypress.env('dispatchUser'),
          disptacherUserPassword: Cypress.env('dispatchUserPassword'),
        });

        cy.newBookingModalHeadingValidation();

        cy.get('.btn').contains('Add a Passenger').should('be.visible').click();

        cy.addPassengerDetails({
          firstName: Cypress.env('passengerFirstName'),
          lastName: Cypress.env('passengerLastName'),
          passengerEmail: Cypress.env('passengerEmail'),
          pickupLoc: 'Bellagio',
          //dropLoc: 'Luxor',
          phoneNumber: Cypress.env('phoneNumber'),
        });
        cy.get('.header-close').click();
      });
      

      it('Select Passenger information from dropdown', () => {
        cy.reload();

        cy.loging({
          dispatcherUser: Cypress.env('dispatchUser'),
          disptacherUserPassword: Cypress.env('dispatchUserPassword'),
        });

        cy.newBookingModalHeadingValidation();

        cy.get('.btn').contains('Add a Passenger').should('be.visible').click();

        cy.selectPassengerDetails({
          firstName: 'testQA testQA',
          pickupLoc: 'Las Vegas',
         // dropLoc: 'Las Vegas Strip',
        });
      });
    });
  });
});