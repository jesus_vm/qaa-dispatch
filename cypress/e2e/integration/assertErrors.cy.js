/// <reference types="cypress"/>

describe('Assert Error Ride ', () => {

    it('Assertion Error Alert In Specific Ride', () => {

        cy.visit(Cypress.env('KaptynSiteUrl'));
        cy.loging({
            dispatcherUser: Cypress.env('dispatchUser'),
            disptacherUserPassword: Cypress.env('dispatchUserPassword'),
        });
        //filter reservation
        cy.get('.search-bar > .form-control').type('RES-14DDQV');
        cy.get('.input-group-addon').click()
        cy.wait(3000);

        // open reservation
         cy.get('.ui-grid-canvas').first().click();
        
        //assertion error alert
        cy.contains('The ride with ', { timeout: 90_000 }).should('be.visible');
    });
});

