describe('Verify Quotes for different veicle', () => {
    Cypress.env('sizes').forEach((size) => {
        it('Verify different quotes', () => {
            cy.visit(Cypress.env('KaptynSiteUrl'));
            cy.loging({
              dispatcherUser: Cypress.env('dispatchUser'),
              disptacherUserPassword: Cypress.env('dispatchUserPassword'),
            });
            cy.newBookingModalHeadingValidation();
            cy.get('.btn').contains('Add a Passenger').should('be.visible').click();
            cy.selectPassengerDetails({
              firstName: 'testQA testQA',
              pickupLoc: 'Luxor Hotel & Casino',
            });
            cy.quotesVerify();
        });
    });
});
