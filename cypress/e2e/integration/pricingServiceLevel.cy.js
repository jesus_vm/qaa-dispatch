/// <reference types="cypress"/>

describe('Pricing Service Level', () => {

    Cypress.env('sizes').forEach((size) => {
        it(`Pricing service level for viewport ${size}`, () => {

            context("Service Level - Premium SUV", () => {
                cy.visit(Cypress.env('KaptynSiteUrl'));

                const serviceLevel = 'Premium SUV Production';
                const taxPrice = 3;

                cy.loging({
                    dispatcherUser: Cypress.env('dispatchUser'),
                    disptacherUserPassword: Cypress.env('dispatchUserPassword')
                });

                cy.get('#navSidebar').find('img').should('be.visible');
                cy.wait(3000)
                // click on pricing
                cy.get('#pricingBtn').contains(' Pricing ').click({ force: true });
                // click on service level
                cy.get('#serviceLevelsBtn').contains(' Service Levels ').click({ force: true });

                cy.get('.form-control').type(serviceLevel);
                cy.wait(3000)
                cy.get('.input-group-addon').click({ force: true });
                cy.wait(3000)
                cy.get('[as-sortable-item=""][style=""] > .list-item > .service-level-list-item-info > .service-level-list-item-name').contains(` ${serviceLevel}`).click({ force: true });
                cy.wait(2000)
                cy.get('.sidelist-items').contains(serviceLevel).should('be.visible');

                // validating the minimum fare not to be null
                cy.get('[name="minimumFare"]').invoke('val').should('not.be.empty');

                // validating the tax value is 3%
                cy.get(':nth-child(14) > .form-control').should('have.value', taxPrice);


            });

        });
    });
});