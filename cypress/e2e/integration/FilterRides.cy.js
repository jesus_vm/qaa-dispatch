/// <reference types="cypress"/>

describe('Filter rides', () => {
    Cypress.env('sizes').forEach((size) => {
        it(`Filter rides for viewport ${size}`, () => {
            cy.visit(Cypress.env('KaptynSiteUrl'));

            cy.loging({
                dispatcherUser: Cypress.env('dispatchUser'),
                disptacherUserPassword: Cypress.env('dispatchUserPassword')
            });

            cy.get('#navSidebar').find('img').should('be.visible');

            //Code for Filter rides
            
            cy.filterRides({
                dispatcheReservationNo: Cypress.env('dispatcherReservationNo'),
              });
        });
    });
});