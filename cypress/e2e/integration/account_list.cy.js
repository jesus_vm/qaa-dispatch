/// <reference types="cypress"/>

describe('Account List', () => {
  Cypress.env('sizes').forEach((size) => {
    it(`Account List for Kops in different viewport ${size}`, () => {
      cy.visit(Cypress.env('KaptynSiteUrl'));

      const extendedAccountPhoneNumber = '+1 902-440-4418';
      const accountphoneNumber = '+19024404418'

      cy.get('.loginForm').contains('Log In').should('be.visible');
      cy.get('.loginIcon').find('img').should('be.visible');

      cy.loging({
        dispatcherUser: Cypress.env('dispatchUser'),
        disptacherUserPassword: Cypress.env('dispatchUserPassword'),
      });

      //Code for Account List
      cy.get('#navSidebar').find('img').should('be.visible');
      cy.get('#corporateBtn').contains('Accounts').click();

      cy.searchAccount({
        accountName: Cypress.env('accountName'),
      });

      //validation for Account name and Phone number
      cy.validateAccountID1(Cypress.env('accountName'));
      cy.wait(2000);
      cy.get('.contact').contains(extendedAccountPhoneNumber);
      cy.get('span.text').contains('Expand').click({ force: true });
      cy.validateAccountID2(Cypress.env('accountID2'));
      cy.get('[name=primaryPhone]').should('have.value', accountphoneNumber);
    });
  });
});
