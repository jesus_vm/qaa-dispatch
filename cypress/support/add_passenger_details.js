/// <reference types="cypress"/>
//function for add passenger details
function addPassengerDetails({
  firstName,
  lastName,
  passengerEmail,
  pickupLoc,
  dropLoc,
  phoneNumber,
}) {
  cy.get('.btn')
    .contains('Search for an existing customer here...')
    .should('be.visible');

  cy.get(
    '.col-md-12 > .form > dr-user-editor.ng-isolate-scope > .no-padding > .row > .col-xs-8 > .intl-tel-input > .form-control'
  ).type(phoneNumber, { force: true });

  cy.get(
    '.col-md-12 > .form > dr-user-editor.ng-isolate-scope > .no-padding > :nth-child(2) > .form-control'
  ).type(firstName);

  cy.get(
    '.col-md-12 > .form > dr-user-editor.ng-isolate-scope > .no-padding > :nth-child(3) > .form-control'
  ).type(lastName);

  cy.get(
    '.col-md-12 > .form > dr-user-editor.ng-isolate-scope > .no-padding > :nth-child(5) > .form-control'
  ).type(passengerEmail);

  cy.get(
    'div.dr-recent-locations-selector > dr-ride-location-selector.ng-isolate-scope > .dr-recent-locations-selector'
  ).type(pickupLoc);

  cy.get('.pac-item-query').contains(pickupLoc).click();


  if(dropLoc){
    cy.get(
      ':nth-child(8) > dr-ride-location-selector.ng-isolate-scope > .dr-recent-locations-selector'
    ).type(dropLoc);

    cy.get('.pac-item-query').contains(dropLoc).click();
  }

  cy.get('.btn').contains('Cancel').click();

  cy.get('.btn').contains('Yes').click();
}
//function for select passenger details from drop down
function selectPassengerDetails({
  firstName,
  pickupLoc,
  dropLoc,
}) {
  cy.get('.btn')
    .contains('Search for an existing customer here...')
    .should('be.visible');

  cy.get(
    '.col-md-12 > .form > dr-user-search-field.ng-isolate-scope > .ui-select-container > .ui-select-match > .btn-default'
  ).type(firstName);
  
  cy.get('#ui-select-choices-row-5-0 > .ui-select-choices-row-inner').click();
  cy.wait(2000);

  cy.get('#reservationModal > .modal-header > .close').click()

  cy.get(
    '[name="firstName"][ng-required="$ctrl.isRequired"][required="required"]'
  ).should('have.value', Cypress.env('passengerFirstName'));

  cy.get(
    '[name="lastName"][ng-required="$ctrl.isRequired"][required="required"]'
  ).should('have.value', Cypress.env('passengerLastName'));

  cy.get(
    'div.dr-recent-locations-selector > dr-ride-location-selector.ng-isolate-scope > .dr-recent-locations-selector'
  ).type(pickupLoc);

  cy.get('.pac-item-query').contains(pickupLoc).click();

  if(dropLoc){
    cy.get(
      ':nth-child(8) > dr-ride-location-selector.ng-isolate-scope > .dr-recent-locations-selector'
    ).type(dropLoc);

    cy.get('.pac-item-query').contains(dropLoc).click();
  }
  
  //add passenger button
  cy.get('[class="btn btn-success"]').click();

}

Cypress.Commands.add('addPassengerDetails', addPassengerDetails);
Cypress.Commands.add('selectPassengerDetails', selectPassengerDetails);
