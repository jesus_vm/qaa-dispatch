/// <reference types="cypress"/>

function filterRides({ dispatcheReservationNo }) {
  cy.get('.search-bar > .form-control').type(dispatcheReservationNo);
  cy.get('.input-group-addon > .glyphicon').click();
  cy.get('.ui-grid-coluiGrid-000K > .ui-grid-cell-contents').should(
    'have.text',
    dispatcheReservationNo
  );
}

Cypress.Commands.add('filterRides', filterRides);
