// <reference types="cypress"/>
function validatereferralcode(companydetailsreferralcode) {
    cy.get('#settingsBtn').click({ force: true });
    cy.get('.input-group > .form-control').should("have.value", companydetailsreferralcode);
}
function validateadminaccount(adminaccount) {
    cy.get('#adminsBtn').click({ force: true });
    cy.get('.form-control.ng-binding > .subtitle ').contains(adminaccount)
        .invoke('text')
        .then(($currentvalue) => {
            expect(adminaccount).to.equals($currentvalue);
        });
}
function validateadmindispatcheraccount(dispatchersaccount) {
    cy.get('#dispatchersBtn').click({ force: true });
    cy.get('.form-control.ng-binding > .subtitle ').contains(dispatchersaccount)
        .invoke('text')
        .then(($currentvalue) => {
            expect(dispatchersaccount).to.equals($currentvalue);
        });
}





Cypress.Commands.add('validatereferralcode', validatereferralcode);
Cypress.Commands.add('validateadminaccount', validateadminaccount);
Cypress.Commands.add('validateadmindispatcheraccount', validateadmindispatcheraccount);
