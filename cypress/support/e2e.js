// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
//example:
//import "./commands.js";

import "./login_commands";
import "./add_passenger_details";
import "./filterRides_commands";
import "./newBooking_commands";
import "./filterRides_commands";
import "./newBookingModal_commands";
import "./account_list_commands";
import "./driver_list_Commands";
import "./billingListUpdate_commands";
import "./customer_list_commands";
import "./Register_commands";
import './editRideStatus_commands';
import "./settings_commands";
import "./quote_verification";
import "./vehicleList_commands";
import "./editRideTime_commands";
import 'cypress-mochawesome-reporter/register';

Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false;
});
