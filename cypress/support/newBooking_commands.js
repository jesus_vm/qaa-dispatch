/// <reference types="cypress"/>
import moment from "moment";

function newBooking({ monthIncrease, day, hour, minute, ToggleTime, serviceLevel, estimateTime, coupon, driver, cardNumber, cardYear, cardMonth, cardCVC, firstName, pickupLoc, dropLoc}) {
    cy.get('#addRiderBtn').click();
    //day default is random
    monthIncrease = monthIncrease || Math.floor(Math.random() * 24) + 1;
    ToggleTime = ToggleTime || "PM";
    day = day || Math.floor(Math.random() * 26) + 1;
    hour = hour || Math.floor(Math.random() * 12) + 1;;
    minute = minute ?? Math.floor(Math.random() * 59) + 1;;
    //service level default
    serviceLevel = serviceLevel || "Premium Sedan Production";
    //additionals default
    estimateTime = estimateTime || "1";
    coupon = coupon || "";
    driver = driver || false;
    //card default
    cardNumber= cardNumber ?? "4111111111111111";
    cardYear= cardYear ?? "2055";
    cardMonth= cardMonth ?? "12";
    cardCVC= cardCVC ?? "123";
    //ride data
    firstName = firstName || 'testQA testQA';
    pickupLoc = pickupLoc || 'Las Vegas';
    dropLoc = dropLoc ?? 'Las Vegas Strip';

    
    //select passenger. need knxg-2353 to refactor this
    
    cy.get('.tab-pane.active > .float-button-container > .btn').click();
    cy.selectPassengerDetails({
        firstName,
        pickupLoc,
        dropLoc,
    });

    cy.wait(3000);
    cy.changePickupTime({
        monthIncrease,
        day,
        hour,
        minute,
        ToggleTime
    });
    cy.setServiceLevel({ serviceLevel });
    cy.setEstimateTime({ estimateTime });
    if( coupon ) cy.setCoupon({ coupon });
    if( cardNumber ) cy.setCard({ cardNumber , cardYear, cardMonth, cardCVC });
    if (driver) cy.setDriver({ driver });
    cy.completeBooking()
    cy.confirmReservation({ monthIncrease, day, hour, minute, ToggleTime });
}

function setCard({ cardNumber, cardYear, cardMonth,cardCVC }) {
    cy.get('#cardNumber').type(cardNumber);
    cy.get('#expirationYear').type(cardYear);
    cy.get('#expirationMonth').type(cardMonth);
    cy.get('#securityCode').type(cardCVC);
}

function setServiceLevel({ serviceLevel }) {
    //serach and select service level
    cy.get('.service-select > .ui-select-match > .btn-default').click();
    cy.get('.service-select > .ui-select-search').type(serviceLevel);
    cy.get('[ng-repeat="$group in $select.groups"][style=""] > #ui-select-choices-row-1-0 > .ui-select-choices-row-inner').click();
}

function setEstimateTime({ estimateTime }) {
    cy.get('#estimatedTime').type(estimateTime);
}

function setCoupon({ coupon }) {
    cy.get('#couponCode').type(coupon);
}

function setDriver({ driver }) {
    cy.wait(3000);
    cy.get('.active > .ng-isolate-scope > .dr-driver-manager > .ui-select-container > .ui-select-match > .btn-default',{timeout:10_000}).click();
    cy.get('.tab-pane.active > .ng-isolate-scope > .dr-driver-manager > .ui-select-container > .ui-select-search').type(driver);
    cy.get('.ui-select-choices-row-inner').first().click();
}

function completeBooking() {
    //get quote
    cy.get('dr-new-booking-quote-button.ng-isolate-scope > .btn-group > .dropdown-toggle')
    cy.wait(10_000);
    //notification
    cy.get('.default-booking').click();
    //should not contains error message
    cy.should('not.contain', "Error");
    
}


function changePickupTime({ monthIncrease, day, hour, minute, ToggleTime }) {

    //monthIncrease 
    cy.get('.datetimePicker > .col-md-6 > .form-control').click();
    cy.wait(1000);

    for (let i = 0; i < monthIncrease; i++) {
        cy.get('button[ng-click="move(1)"]').click();
    }
    cy.get('.btn > .ng-binding').contains(day).click();

    cy.get('.hours > .form-control').clear();
    cy.get('.hours > .form-control').type(hour);

    cy.get('.minutes > .form-control').clear();
    cy.get('.minutes > .form-control').type(minute);

    //toggle PM or AM
    cy.get('.am-pm > .btn').invoke('text').then((timeFrame) => {
        if (timeFrame != ToggleTime) {
            cy.get('.am-pm > .btn').click();
        }
    })

}

function confirmReservation({ monthIncrease, day, hour, minute, ToggleTime }) {
    //cy.wait(60_000);
    cy.contains('New reservation ',{timeout:90_000}).should('be.visible').invoke('text').then((text) => {
     
        const alertDate = moment(text);
        const res = text.split(' ')[2].split('-')[1] .replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
        //filter reservation
        cy.get('.search-bar > .form-control').type('RES-'+res);
        cy.get('.input-group-addon').click()

        // open reservation
        cy.get('.ui-grid-canvas').first().click();
        //get reservation number
        cy.get('.title > .ng-binding').invoke('text').then((reservationNumber) => {
            expect(reservationNumber.includes("RES-")).to.be.true;
        })

    })
     
}

Cypress.Commands.add('confirmReservation', confirmReservation);
Cypress.Commands.add('changePickupTime', changePickupTime)
Cypress.Commands.add('setServiceLevel', setServiceLevel)
Cypress.Commands.add('setEstimateTime', setEstimateTime)
Cypress.Commands.add('setCoupon', setCoupon)
Cypress.Commands.add('setDriver', setDriver)
Cypress.Commands.add('setCard', setCard)
Cypress.Commands.add('completeBooking', completeBooking)
Cypress.Commands.add('newBooking', newBooking)
