// <reference types="cypress"/>

function customerList({ searchEmail }) {
  cy.get('#customersBtn').click();
  cy.get('#affiliate_search_bar').type(searchEmail)
  cy.wait(3000);
  cy.get('.input-group-addon > .glyphicon').click({ force: true })
  cy.wait(3000);
  cy.get('.ui-grid-viewport .ui-grid-canvas').click()
  cy.wait(3000);
}
function validatCustomerPhoneNumber(dispatchPhoneNumber) {
  cy.get('input[name=phoneNumber]').should("have.value", dispatchPhoneNumber);
}
function validateCustomerLastName(lastName) {
  cy.get('.no-padding > :nth-child(3) > .form-control').should("have.value", lastName);
}


Cypress.Commands.add('customerList', customerList);
Cypress.Commands.add('validatCustomerPhoneNumber', validatCustomerPhoneNumber);
Cypress.Commands.add('validateCustomerLastName', validateCustomerLastName);