function quotesVerify() {

  // the space in price is important
  const serviceLevels = [
    { name: 'Luxe SUV XL Production', price: ' $72.10 ' },
    { name: 'Luxe Sedan Production', price: ' $61.80 ' },
    { name: 'Premium Sedan Production', price: ' $46.35 ' },
  ]

  serviceLevels.forEach((serviceLevel) => {
    cy.get('.service-select > .ui-select-match > .btn-default').click();
    cy.get('[class="ng-binding ng-scope"]').contains(serviceLevel.name).click({ force: true });
    cy.get('.btn-group > .dropdown-toggle > .fa').click();
    cy.wait(10000);
    cy.get('[ng-if="$ctrl.quote.validEstimate"]').should("have.text", serviceLevel.price);
    cy.wait(2000);
  });

}
Cypress.Commands.add('quotesVerify', quotesVerify);