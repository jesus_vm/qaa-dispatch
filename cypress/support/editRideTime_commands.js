/// <reference types="cypress"/>

function filterRides({ dispatcheReservationNo }) {
    //click status unassigned checkbox
    cy.get('.filter-type > .ng-binding').click();
    cy.get('input[type=checkbox]').uncheck().should('not.be.checked')
    cy.wait(5000);
    cy.get('.completed > .ng-binding').click();
    cy.wait(5000);
    cy.get('.search-bar > .form-control').type(dispatcheReservationNo);
    cy.get('.input-group-addon > .glyphicon').click({ force: true });
    cy.wait(5000);
    cy.get('.ui-grid-canvas > .ng-scope').click({ multiple: true });
    cy.wait(2000);
}
function changeRidePickupTime({ updatedhour }) {
    cy.get('.hours > .form-control').clear();
    cy.get('.hours > .form-control').type(updatedhour);
}
function validateUpdatedTime(updatedhour) { ///uppercases
    cy.get('.hours > .form-control').should("have.value", updatedhour);
}


Cypress.Commands.add('filterRides', filterRides);
Cypress.Commands.add('changeRidePickupTime', changeRidePickupTime);
Cypress.Commands.add('validateUpdatedTime', validateUpdatedTime);
