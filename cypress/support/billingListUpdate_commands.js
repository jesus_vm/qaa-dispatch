function filterBilling({ ReservationNo }) {
    cy.get('.search-bar > .form-control').type(ReservationNo);
    cy.get('.input-group-addon > .glyphicon').click();
    cy.wait(3000);
    cy.get('.ui-grid-viewport .ui-grid-canvas').click();
    cy.wait(3000);

    cy.get('div.col-md-12 > h4.ng-binding').should("have.text", 'Ride Summary');
    cy.get('.payment-status-badge').should("have.text", ' Unpaid ');

}

Cypress.Commands.add('filterBilling', filterBilling);