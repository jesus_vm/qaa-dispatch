/// <reference types="cypress"/>

import moment from "moment";

function newBookingModalHeadingValidation(){
    cy.get('#addRiderBtn').should('be.visible');
    cy.get('#addRiderBtn').click({ force: true });
    cy.wait(1000);
    cy.get('[ng-show="!addRideCtrl.specialTitle"]').contains('New Booking').should('have.text', "New Booking");
}

Cypress.Commands.add('newBookingModalHeadingValidation', newBookingModalHeadingValidation);