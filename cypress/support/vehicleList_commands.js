// <reference types="cypress"/>

function vehicleList({ licensePlate }) {
    cy.get('#vehiclesBtn').click();
    cy.wait(3000)
    cy.get('.search-bar > .form-control').type(licensePlate).type('{enter}');
  
    cy.wait(3000);
    cy.get('.ui-grid-viewport .ui-grid-canvas').click()
    cy.wait(3000);
    cy.get('[name="licensePlate"]').should("have.value", licensePlate);
}
function validateMake(make) {
 cy.get('[name="make"]').should( "have.value", make );
 
}


Cypress.Commands.add('vehicleList', vehicleList);
Cypress.Commands.add('validateMake', validateMake);