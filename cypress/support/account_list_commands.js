/// <reference types="cypress"/>
//Reusable code for Account Search

function searchAccount({ accountName }) {
  cy.get('input[class^=form-control][type=text]').type(accountName);
  cy.get('i[class$=glyphicon-search]').click();
}
//Validation for Account name
Cypress.Commands.add('validateAccountID1', (accountName) => {
  cy.get('h4.ng-binding')
    .contains('Circa - Testing')
    .invoke('text')
    .then(($currentvalue) => {
      expect(accountName).to.equals($currentvalue);
    });
});

//Validation for new popup Account name
Cypress.Commands.add('validateAccountID2', (accountID2) => {
  cy.get('.title > .ng-binding')
    .contains('Circa - Testing ')
    .invoke('text')
    .then(($currentvalue) => {
      expect(accountID2).to.equals($currentvalue);
    });
});

Cypress.Commands.add('searchAccount', searchAccount);
