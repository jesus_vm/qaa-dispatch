/// <reference types="cypress"/>

var emailid;
var today;
var id;
function admin({
    dispatcheremailstart,
    dispatcheremailend


}) {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    today = mm + dd;

    const uuid = () => Cypress._.random(0, 1e3)
    const id = uuid()

    emailid = dispatcheremailstart + id + today + dispatcheremailend;
    cy.get('[name=administratorWhitelistEmail]').type(emailid)

    cy.get('i[class$=glyphicon-plus]').click();
    cy.wait(2000);

}

//Create a new Account
function addadminDetails({
    
    dispatcheremailstart,
    dispatchLastname,
    dispatchphonedialcode,
    dispatchPassword,
    dispatchConfirmPassword
}) {
    cy.get('#userFirstName').clear();
    cy.get('#userFirstName').type(dispatcheremailstart, { force: true });

    cy.get('#userLastName').clear();
    cy.get('#userLastName').type(dispatchLastname, { force: true });
    
    const uuid = () => Cypress._.random(0, 1e4)
    const id = uuid()
    const phonenum = dispatchphonedialcode + id;
    cy.get('#userPhone').clear();
    cy.get('#userPhone').type(phonenum, { force: true });


    cy.get('#email').clear();
    cy.get('#email').type(emailid, { force: true });

    cy.get('#password').clear();
    cy.get('#password').type(dispatchPassword, { force: true });

    cy.get('#passwordConfirm').clear();
    cy.get('#passwordConfirm').type(dispatchConfirmPassword, { force: true });
    cy.wait(1000);

    cy.get('#aggrementCheckBox').click({ force: true });
    cy.wait(2000);
    cy.get('#finishBtn').contains(' Next ').click({ force: true });
    

}
//Validation for Thank you for signing up!
Cypress.Commands.add("validatePopUpMessage", (popupMessage) => {

    
    cy.get('.ng-binding.ng-scope').contains(' Thank you for signing up! ')
        .invoke('text')
        .then(($currentvalue) => {

            expect(popupMessage).to.equals($currentvalue);
        })
})
//Login with new data
function logingnew({
    
    dispatchPassword
}) {
    
    cy.get('#email').clear();
    cy.get('#email').type(emailid , { force: true });
    
    cy.get('#password').clear();
    cy.get('#password').type(dispatchPassword)


    cy.get('#loginBtn').click();
}
//Validation for Login successfully
Cypress.Commands.add("validateloginsuccessfully", (Homepage) => {

    cy.get('#groupSwitcher').contains(' Kaptyn Las Vegas ')

        .invoke('text')
        .then(($currentvalue) => {

            expect(Homepage).to.equals($currentvalue);
        })
        cy.get('#drUserAccountMenuBtn > .ng-binding').click();
        cy.get('.dropdown-menu > :nth-child(3) > .ng-binding').click();
        cy.wait(2000);
        cy.get('#email').clear();
    })
function Verifyemailpresentassertion()

{   
    var j=0;
    let countOfEle =  0;
    cy.get('[class="subtitle ng-binding"]').then($elements => {
        countOfEle = $elements.length;
        var currentvalue1;
         for(let i=2; i <= countOfEle ; i++)
         {
            cy.log(i);
        cy.get(':nth-child(1) > .panel > .panel-body > :nth-child('+i+') > .form-control >.subtitle').invoke('text')
        .then(($currentvalue) => {
            cy.log($currentvalue);
            cy.log(emailid.toLowerCase());
              if($currentvalue == emailid.toLowerCase())
              { cy.log('in if condition');
              cy.log(i);
                j=cy.log(i);
                
                  cy.log('in if condition');
                  expect($currentvalue).to.equals(emailid.toLowerCase());
                  cy.log($currentvalue);
                  cy.get(':nth-child(1) > .panel > .panel-body > :nth-child('+i+') > .input-group-btn > .btn').click();
                   
              }
            })
             
        }
         
    })
    cy.log('out of for loop');
    cy.log(j);
}
    

Cypress.Commands.add('admin', admin)
Cypress.Commands.add('addadminDetails', addadminDetails)
Cypress.Commands.add('logingnew', logingnew)
Cypress.Commands.add('Verifyemailpresentassertion', Verifyemailpresentassertion)