// <reference types="cypress"/>

function driverList({ searchByEmail }) {
  cy.get('#driversBtn').click();
  cy.get('.form-control').type(searchByEmail)
  cy.wait(3000);
  cy.get('.input-group-addon > .glyphicon').click({ force: true })
  cy.wait(3000);
  cy.get('.ui-grid-viewport .ui-grid-canvas').click()
  cy.wait(3000);
}
function validatDriverPhoneNumber(dispatchPhoneNumber) {
  cy.get("#phone").should( "have.value", dispatchPhoneNumber );
}

function validateDriverLastName(lastName) {
  cy.get('#lastName').should( "have.value", lastName );
}


Cypress.Commands.add('validatDriverPhoneNumber', validatDriverPhoneNumber);
Cypress.Commands.add('validateDriverLastName', validateDriverLastName);
Cypress.Commands.add('driverList', driverList);