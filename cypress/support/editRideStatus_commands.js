function changeRideStatus() {

    cy.get('.ride-status-dropdown').find('option:selected').should('have.text', ' Scheduled ');
    cy.get('.ride-status-dropdown').select('--en_route');
    cy.wait(15000)
    cy.reload();
    cy.get('.ride-status-dropdown').find('option:selected').should('have.text', ' En Route ');


    cy.get('.ride-status-dropdown', { timeout: 60_000 }).should('be.visible').select(`Arrived`);
    cy.wait(15000)
    cy.reload();
    cy.get('.ride-status-dropdown').find('option:selected').should('have.text', ' Arrived ');

    cy.get('.ride-status-dropdown', { timeout: 60_000 }).should('be.visible').select('--in_progress');
    cy.wait(15000)
    cy.reload();
    cy.get('.ride-status-dropdown').find('option:selected').should('have.text', ' In Progress ');

    cy.get('.ride-status-dropdown', { timeout: 60_000 }).should('be.visible').select('--completed');
    cy.wait(15000)
    cy.reload();
    cy.get('.ride-status-dropdown').find('option:selected').should('have.text', ' Completed ');

}
  
  Cypress.Commands.add('changeRideStatus', changeRideStatus);